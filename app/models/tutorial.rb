class Tutorial < ApplicationRecord
  validates :title, presence: true, length: { minimum: 5 }
  validates :body, presence: true
  validates :tutorial_link, presence: true
end
