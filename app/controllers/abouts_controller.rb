class AboutsController < ApplicationController
  before_action :set_about, only: [:show, :edit, :update, :destroy]

  def index
    @pagy, @abouts = pagy(About.all.order('created_at asc'), items: 1)
  end

  def show
    @about = About.find(params[:id])
    @abouts = About.order('created_at asc').limit(4).offset(1)
  end

  private
    def set_about
      @about = About.find(params[:id])
    end

    def about_params
      params.require(:about).permit(:title, :body)
    end
end
