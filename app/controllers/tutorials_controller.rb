class TutorialsController < ApplicationController
  before_action :set_tutorial, only: [:show, :edit, :update, :destroy]

  def index
    @tutorials = Tutorial.all
    @pagy, @tutorials = pagy(Tutorial.all.order('created_at DESC'), items: 4)
  end

  def show
    @tutorial = Tutorial.find(params[:id])
    @tutorials = Tutorial.order('created_at desc').limit(4).offset(1)
  end

  private
    def set_tutorial
      @tutorial = Tutorial.find(params[:id])
    end

    def tutorial_params
      params.require(:tutorial).permit(:title, :body, :tutorial_link)
    end
end
