class ContactsController < ApplicationController
  before_action :set_contact, only: [:show, :edit, :update, :destroy]

  def index
    @contacts = Contact.all
  end

  def show
    @contact = Contact.find(params[:id])
    @contacts = Contact.order('created_at desc').limit(4).offset(1)
  end

  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)

    respond_to do |format|
      if @contact.save
        format.html do
          flash[:success] = 'Message Sent!'
          redirect_to contacts_url
        end
      else
        format.html { flash[:failure!] = 'Message Not Sent! Verify Details & Send Again!' }
        format.html { render :new }
      end
    end
  end

  private
    def set_contact
      @contact = Contact.find(params[:id])
    end

    def contact_params
      params.require(:contact).permit(:email, :message)
    end
end
