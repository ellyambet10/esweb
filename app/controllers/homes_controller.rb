class HomesController < ApplicationController
  before_action :set_home, only: [:show, :edit, :update, :destroy]

  def index
    @pagy, @homes = pagy(Home.all.order('created_at DESC'), items: 8)
  end

  def show
    @home = Home.find(params[:id])
    @homes = Home.order('created_at desc').limit(4).offset(1)
  end

  private
  def set_home
    @home = Home.find(params[:id])
  end

  def home_params
    params.require(:home).permit(:title, :body, :demo_link, :download_link)
  end
end