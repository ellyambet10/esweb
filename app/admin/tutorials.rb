ActiveAdmin.register Tutorial do
  permit_params :title, :body, :tutorial_link
  show do |_t|
    attributes_table do
      row :title
      row :body
      row :tutorial_link
    end
  end

  form html: { enctype: 'multipart/form-data' } do |f|
    f.inputs do
      f.input :title
      f.input :body
      f.input :tutorial_link
    end
    f.actions
  end
end
