json.extract! home, :id, :title, :body, :demo_link, :download_link, :created_at, :updated_at
json.url home_url(home, format: :json)
