json.extract! tutorial, :id, :title, :body, :tutorial_link, :created_at, :updated_at
json.url tutorial_url(tutorial, format: :json)
