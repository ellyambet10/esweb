class CreateHomes < ActiveRecord::Migration[5.2]
  def change
    create_table :homes do |t|
      t.string :title
      t.text :body
      t.string :demo_link
      t.string :download_link

      t.timestamps
    end
  end
end
